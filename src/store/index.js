import Vue from 'vue'
import Vuex from 'vuex'
import * as types from './mutation-types'
import axios from 'axios'
import { auth } from './auth.module';



Vue.use(Vuex);

Vue.use(Vuex)

export default new Vuex.Store({

  
  
  state: {
    number: 0,
    listdata: [],
    name:'',
    form :{},
    setstatus:[],
    slogin:false,
    id:''
  
   // MASSAGE: null
  },
  mutations: {
    //set name of collection to use
    setName(state, newName) {
      state.name = newName;
    },

    setLogin(state,status) {
      state.slogin = status;
      console.log(state.slogin)
    },
    //set name of collection to use
     setform(state, newform) {
      state.form = newform;
    },
    //set name of collection to use
    setid(state, id) {
      state.id = id;
    },
     //get data 
    [types.getData](state){
      axios
        .get('http://localhost:8080/api/test/'+this.state.name)
        .then(res => {
          console.log(res)
          state.listdata = res.data 
 
        })
        .catch(err => {
          console.log(err)
        })


    },
    // insert data 
    [types.insertData](){
      axios
      .post('http://localhost:8080/api/test/add/'+this.state.name,this.state.form)
      .then(res => {
        console.log(res)
        this.commit(types.getData)
       
      })
      .catch(err => {
        console.log(err)
      })
    },
    //update data
    [types.updateData](state){
      console.log("formmm",state.form)
      
      axios
      .put('http://localhost:8080/api/test/update/'+this.state.name,this.state.form)
      .then(res => {
        console.log(res)
        this.commit(types.getData)
    
       
      })
      .catch(err => {
        console.log(err)
      })
    },


    //delete data
    [types.delelteData](){
      axios
      .delete('http://localhost:8080/api/test/del/'+this.state.name+'/'+this.state.id)
      .then(res => {
        console.log(res)
        this.commit(types.getData)
      })
      .catch(err => {
        console.log(err)
      })
    }




    

   
  },
  actions: {
    SET_NAME(context, newName) {
      context.commit("setName", newName);
      console.log(this.state.name)
    },
    SET_FORM(context, newform) {
      context.commit("setform", newform);
      console.log("form",this.state.form)
    },
    SET_ID(context, id) {
      context.commit("setid", id);
      console.log("id",this.state.id)
    },
    SET_LOGIN(context,status) {
      context.commit("setLogin",status);
      

    },
    getData({ commit }) {
      commit(types.getData)
     
    },
    MapRole({ commit }) {
      commit('MapRole')
     
    },
    insertData({ commit }) {
      commit(types.insertData)
     
    },
    updateData({ commit }) {
      commit(types.updateData)
     },
     delelteData({ commit }) {
      commit(types.delelteData)
     },
   
  },
  modules: {
    auth
  }
})
